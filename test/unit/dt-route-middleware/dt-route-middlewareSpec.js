'use strict';

describe('', function() {

  var module;
  var dependencies;
  dependencies = [];

  var hasModule = function(module) {
  return dependencies.indexOf(module) >= 0;
  };

  beforeEach(function() {

  // Get module
  module = angular.module('dt-route-middleware');
  dependencies = module.requires;
  });

  it('should load config module', function() {
    expect(hasModule('dt-route-middleware.config')).to.be.ok;
  });

  

  

  
  it('should load services module', function() {
    expect(hasModule('dt-route-middleware.services')).to.be.ok;
  });
  

  

});
