/**
 * Created by coichedid on 21/04/15.
 */
'use strict';

angular.module('dt-route-middleware').run( [ 
  '$rootScope',
  '$location',
  'RouteMiddleware',
  function( $rootScope , $location , RouteMiddleware )
  {
      $rootScope.$on('$routeChangeStart' , function( event , next , current ){



          var middleware = function(){
              var promise = RouteMiddleware.handle( next );


              promise.catch( function( e ){
                  console.log('middleware has caught an error of: ' , e);
                  $location.path('/login').replace();
                  $rootScope.$apply;
              } );

              return promise;

          }

          next.resolve = {
              middleware : middleware
          }


      });

      $rootScope.$on('$routeChangeSuccess' , function( event , current , previous  ){
          console.log('route changed' , event , current , previous );
      });

      $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {

          console.log( 'error happened' );
      });



} ]);