/**
 * Created by coichedid on 21/04/15.
 */
'use strict';

angular.module('dt-route-middleware').service('RouteMiddlewareHandler', [ '$q' ,
  function($q) {
    // Route middleware handler service logic
    // ...

    // Public API
    this.handle = function( middleware , route  , handlers )
    {

      var self = this;

      var mw = middleware;

      self.$handlers = handlers;


        var test = angular.isArray([]);

      console.log('handling' , mw);

      var promise = $q( function( resolve , reject )
          { 
             
              if( mw === false || mw === undefined || mw === null )
              {
                  resolve('no middleware property provided');
                  return;
              }


              if( mw.public !== undefined )
              {
                  resolve('no middleware callbacks are executed on a public route');
                  return;
              }


              Object.keys( self.$handlers ).forEach( function( key ){
                if( mw[key] !== undefined )
                {
                    self.$handlers[key].forEach( function( obj )
                    {
                      try
                      {
                          obj( mw ,  route );
                      }
                      catch( e )
                      {
                        reject( e );
                        return;
                      }
                    });
                }
              } );

              resolve('all is ok with middleware');

          } );


        return promise;

    };


  }
]);
