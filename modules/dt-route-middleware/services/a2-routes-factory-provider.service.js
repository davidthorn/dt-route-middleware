/**
 * Created by coichedid on 21/04/15.
 */
'use strict';


angular.module('dt-route-middleware').provider('RoutesFactory' , RoutesFactory  );

RoutesFactory.$inject = ['$routeProvider'];

function RoutesFactory( $routeProvider )
{



    var self = this;

    this.$routes = [];

    this.$params = {};

    this.init = function( params )
    {
        self.$params = params;
    };

   
    this.otherwise = function( params )
    {
      $routeProvider.otherwise( params );
    };

    this.addRouteToRouteProvider = function( route , params )
    {
        self.$routes.push(  self.addRoute( route , params ) );
        $routeProvider.when( route , params );
    }; 

    this.addRoute = function( route , params )
    {
        var $params = params;
        if( params.url === undefined )
        { 
            $params.url = route;
        }

        return self.addMiddleware($params);
    };

    this.addMiddleware = function( params  )
    {
        var $params = params;

        if( $params.middleware === undefined )
        {
            $params.middleware = {};
        }

        return $params;

    };

    this.addAuthenticationMiddleware = function( params , options )
    {
        var $params = self.addMiddleware( params );
        if(  $params.middleware.authentication === undefined  )
        {
            $params.middleware.authentication = {};
        }

        if( angular.isObject( options ) &&  !angular.isArray( options ))
        {
            console.log('options is an object' , options);
        }

        return $params;

    };

    this.addAuthorisationMiddleware = function( params )
    {
        var $params = self.addMiddleware( params );
        if(  $params.middleware.authorisation === undefined  )
        {
            $params.middleware.authorisation = {};
        }

        return $params;

    };

    this.addPublicMiddleware = function( params )
    {
        var $params = self.addMiddleware( params );
        
        if(  $params.middleware.public === undefined  )
        {
            $params.middleware.public = {};
        }

        return $params;

    };


    this.$get = function()
    {};

    this.getRoutes = function(){
        return self.$routes;
    };

}

