/**
 * Created by coichedid on 21/04/15.
 */
'use strict';

angular.module('dt-route-middleware').config( RouteMiddlewareConfigController );

RouteMiddlewareConfigController.$inject = [ 'RouteMiddlewareProvider' ];

function RouteMiddlewareConfigController( RouteMiddlewareProvider )
{
    RouteMiddlewareProvider.init( {

    } );
}