/**
 * Created by coichedid on 21/04/15.
 */
'use strict';

angular.module('dt-route-middleware').provider('RouteAdaptor', RouteAdaptor );

RouteAdaptor.$inject = [];

function RouteAdaptor(  )
{

    var RoutesFactoryProvider;

    this.$get = function()
    {

    };



    this.init = function( $routesFactoryProvider )
    {
        RoutesFactoryProvider = $routesFactoryProvider;
    };


    this.interface = {


        public: function( route , params  )
        {
              var $params = RoutesFactoryProvider.addPublicMiddleware( params );
              RoutesFactoryProvider.addRouteToRouteProvider( route , $params );
        },


        authentication : function( route , params )
        {
            var $params = RoutesFactoryProvider.addAuthenticationMiddleware( params );
            RoutesFactoryProvider.addRouteToRouteProvider( route ,  $params );
        },


        authorisation : function( route , params )
        {
            var $params = RoutesFactoryProvider.addAuthorisationMiddleware( params );
            RoutesFactoryProvider.addRouteToRouteProvider( route ,   $params );
        },


        otherwise : function( params )
        {
            RoutesFactoryProvider.otherwise( params );
        },


        getRoutes: function()
        {
           return RoutesFactoryProvider.getRoutes();
        }

    };


}

