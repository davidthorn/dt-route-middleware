/**
 * Created by coichedid on 21/04/15.
 */
'use strict';

 angular.module('dt-route-middleware').provider( 'RouteMiddleware' ,  RouteMiddlewareProvider );

RouteMiddlewareProvider.$inject = [  'RouteAdaptorProvider' , 'RoutesFactoryProvider' ];


function RouteMiddlewareProvider( RouteAdaptorProvider , RoutesFactoryProvider  )
{

    var self = this;

    var adaptor = RouteAdaptorProvider;


    this.$params = {};

    this.$routes = [];

    this.init = function( params )
    {
        self.$params = params;
    };


    console.log(adaptor);

    adaptor.init( RoutesFactoryProvider );


    // abstract version of $routeProvider so as that we can catch the route information
    this.when = function( route , params )
    {
        adaptor.interface.public( route , params );
        return self;
    };

    
    this.authentication = function( route , params  , options )
    {
        adaptor.interface.authentication( route , params );
        return self;
    };

    this.authorisation = function( route , params )
    {
        adaptor.interface.authorisation( route , params );
        return self;
    };

    this.public = function( route , params )
    {
        adaptor.interface.public( route , params );
        return self;
    };

    // abstract version of $routeProvider so as that we can catch the route information
    this.otherwise = function( params )
    {
        adaptor.interface.otherwise( params );
        return self;
    };




    this.$get = [ 'RouteMiddlewareHelper' ,  function( RouteMiddlewareHelper ){
        self.$params.routes = RoutesFactoryProvider.getRoutes();
        RouteMiddlewareHelper.init( self.$params );
        return RouteMiddlewareHelper;
    }];
   
    
}