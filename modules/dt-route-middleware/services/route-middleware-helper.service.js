/**
 * Created by coichedid on 21/04/15.
 */
'use strict';

 angular.module('dt-route-middleware').service( 'RouteMiddlewareHelper' ,  RouteMiddlewareHelper );

  RouteMiddlewareHelper.$inject = [ 'RouteMiddlewareHandler' ];

  function RouteMiddlewareHelper( RouteMiddlewareHandler )
  {
      
      var self = this;
      
      this.$handlers = {};

      this.$params = {};

      this.init = function( params )
      {
          this.$params = params;
      };



      this.register = function(  middleware ,   $routeMiddlewareHandler )
      {

          if( angular.isArray( $routeMiddlewareHandler )  )
          {
              $routeMiddlewareHandler.forEach( function(t){
                  self.register( middleware , t);
              } );
              return;
          }


          if( self.$handlers[middleware] === undefined )
          {
              self.$handlers[middleware] = [];
          }

          self.$handlers[middleware].push(  $routeMiddlewareHandler  );
          
      };


      this.hasMiddleware = function( route ){

            if( route === undefined )
            {
                return false;
            }


            if( route.$$route === undefined )
            {
                return false;
            }


            var $route = route.$$route;

            if( $route.middleware === undefined )
            {
                return false;
            }

            var mw = $route.middleware;

            return mw;

      };

      this.handle = function( route )
      {

         var mw = self.hasMiddleware( route );
         return RouteMiddlewareHandler.handle( mw , route , self.$handlers );

      };

     
  }

