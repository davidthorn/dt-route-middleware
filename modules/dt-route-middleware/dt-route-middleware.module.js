(function (angular) {

  // Create all modules and define dependencies to make sure they exist
  // and are loaded in the correct order to satisfy dependency injection
  // before all nested files are concatenated by Gulp

  // Config
  angular.module('dt-route-middleware.config', [])
      .value('dt-route-middleware.config', {
          debug: true
      });

  // Modules
  
  angular.module('dt-route-middleware.services', []);
  
  angular.module('dt-route-middleware',
      [
        'dt-route-middleware.config',
        'dt-route-middleware.services',
        'ngRoute'
      ]);


})(angular);
