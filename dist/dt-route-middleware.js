(function (angular) {

  // Create all modules and define dependencies to make sure they exist
  // and are loaded in the correct order to satisfy dependency injection
  // before all nested files are concatenated by Gulp

  // Config
  angular.module('dt-route-middleware.config', [])
      .value('dt-route-middleware.config', {
          debug: true
      });

  // Modules
  
  angular.module('dt-route-middleware.services', []);
  
  angular.module('dt-route-middleware',
      [
        'dt-route-middleware.config',
        'dt-route-middleware.services',
        'ngRoute'
      ]);


})(angular);

/**
 * Created by coichedid on 21/04/15.
 */
'use strict';

angular.module('dt-route-middleware').provider('RouteAdaptor', RouteAdaptor );

RouteAdaptor.$inject = [];

function RouteAdaptor(  )
{

    var RoutesFactoryProvider;

    this.$get = function()
    {

    };



    this.init = function( $routesFactoryProvider )
    {
        RoutesFactoryProvider = $routesFactoryProvider;
    };


    this.interface = {


        public: function( route , params  )
        {
              var $params = RoutesFactoryProvider.addPublicMiddleware( params );
              RoutesFactoryProvider.addRouteToRouteProvider( route , $params );
        },


        authentication : function( route , params )
        {
            var $params = RoutesFactoryProvider.addAuthenticationMiddleware( params );
            RoutesFactoryProvider.addRouteToRouteProvider( route ,  $params );
        },


        authorisation : function( route , params )
        {
            var $params = RoutesFactoryProvider.addAuthorisationMiddleware( params );
            RoutesFactoryProvider.addRouteToRouteProvider( route ,   $params );
        },


        otherwise : function( params )
        {
            RoutesFactoryProvider.otherwise( params );
        },


        getRoutes: function()
        {
           return RoutesFactoryProvider.getRoutes();
        }

    };


}


/**
 * Created by coichedid on 21/04/15.
 */
'use strict';


angular.module('dt-route-middleware').provider('RoutesFactory' , RoutesFactory  );

RoutesFactory.$inject = ['$routeProvider'];

function RoutesFactory( $routeProvider )
{



    var self = this;

    this.$routes = [];

    this.$params = {};

    this.init = function( params )
    {
        self.$params = params;
    };

   
    this.otherwise = function( params )
    {
      $routeProvider.otherwise( params );
    };

    this.addRouteToRouteProvider = function( route , params )
    {
        self.$routes.push(  self.addRoute( route , params ) );
        $routeProvider.when( route , params );
    }; 

    this.addRoute = function( route , params )
    {
        var $params = params;
        if( params.url === undefined )
        { 
            $params.url = route;
        }

        return self.addMiddleware($params);
    };

    this.addMiddleware = function( params  )
    {
        var $params = params;

        if( $params.middleware === undefined )
        {
            $params.middleware = {};
        }

        return $params;

    };

    this.addAuthenticationMiddleware = function( params , options )
    {
        var $params = self.addMiddleware( params );
        if(  $params.middleware.authentication === undefined  )
        {
            $params.middleware.authentication = {};
        }

        if( angular.isObject( options ) &&  !angular.isArray( options ))
        {
            console.log('options is an object' , options);
        }

        return $params;

    };

    this.addAuthorisationMiddleware = function( params )
    {
        var $params = self.addMiddleware( params );
        if(  $params.middleware.authorisation === undefined  )
        {
            $params.middleware.authorisation = {};
        }

        return $params;

    };

    this.addPublicMiddleware = function( params )
    {
        var $params = self.addMiddleware( params );
        
        if(  $params.middleware.public === undefined  )
        {
            $params.middleware.public = {};
        }

        return $params;

    };


    this.$get = function()
    {};

    this.getRoutes = function(){
        return self.$routes;
    };

}


/**
 * Created by coichedid on 21/04/15.
 */
'use strict';

 angular.module('dt-route-middleware').provider( 'RouteMiddleware' ,  RouteMiddlewareProvider );

RouteMiddlewareProvider.$inject = [  'RouteAdaptorProvider' , 'RoutesFactoryProvider' ];


function RouteMiddlewareProvider( RouteAdaptorProvider , RoutesFactoryProvider  )
{

    var self = this;

    var adaptor = RouteAdaptorProvider;


    this.$params = {};

    this.$routes = [];

    this.init = function( params )
    {
        self.$params = params;
    };


    console.log(adaptor);

    adaptor.init( RoutesFactoryProvider );


    // abstract version of $routeProvider so as that we can catch the route information
    this.when = function( route , params )
    {
        adaptor.interface.public( route , params );
        return self;
    };

    
    this.authentication = function( route , params  , options )
    {
        adaptor.interface.authentication( route , params );
        return self;
    };

    this.authorisation = function( route , params )
    {
        adaptor.interface.authorisation( route , params );
        return self;
    };

    this.public = function( route , params )
    {
        adaptor.interface.public( route , params );
        return self;
    };

    // abstract version of $routeProvider so as that we can catch the route information
    this.otherwise = function( params )
    {
        adaptor.interface.otherwise( params );
        return self;
    };




    this.$get = [ 'RouteMiddlewareHelper' ,  function( RouteMiddlewareHelper ){
        self.$params.routes = RoutesFactoryProvider.getRoutes();
        RouteMiddlewareHelper.init( self.$params );
        return RouteMiddlewareHelper;
    }];
   
    
}
/**
 * Created by coichedid on 21/04/15.
 */
'use strict';

angular.module('dt-route-middleware').config( RouteMiddlewareConfigController );

RouteMiddlewareConfigController.$inject = [ 'RouteMiddlewareProvider' ];

function RouteMiddlewareConfigController( RouteMiddlewareProvider )
{
    RouteMiddlewareProvider.init( {

    } );
}
/**
 * Created by coichedid on 21/04/15.
 */
'use strict';

angular.module('dt-route-middleware').run( [ 
  '$rootScope',
  '$location',
  'RouteMiddleware',
  function( $rootScope , $location , RouteMiddleware )
  {
      $rootScope.$on('$routeChangeStart' , function( event , next , current ){



          var middleware = function(){
              var promise = RouteMiddleware.handle( next );


              promise.catch( function( e ){
                  console.log('middleware has caught an error of: ' , e);
                  throw 'promblem occured';
                  $location.path('/login').replace();
                  $rootScope.$apply;
              } );

              return promise;

          }

          next.resolve = {
              middleware : middleware
          }


      });

      $rootScope.$on('$routeChangeSuccess' , function( event , current , previous  ){
          console.log('route changed' , event , current , previous );
      });

      $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {

          console.log( 'error happened' );
      });



} ]);
/**
 * Created by coichedid on 21/04/15.
 */
'use strict';

angular.module('dt-route-middleware').service('RouteMiddlewareHandler', [ '$q' ,
  function($q) {
    // Route middleware handler service logic
    // ...

    // Public API
    this.handle = function( middleware , route  , handlers )
    {

      var self = this;

      var mw = middleware;

      self.$handlers = handlers;


        var test = angular.isArray([]);

      console.log('handling' , mw);

      var promise = $q( function( resolve , reject )
          { 
             
              if( mw === false || mw === undefined || mw === null )
              {
                  resolve('no middleware property provided');
                  return;
              }


              if( mw.public !== undefined )
              {
                  resolve('no middleware callbacks are executed on a public route');
                  return;
              }


              Object.keys( self.$handlers ).forEach( function( key ){
                if( mw[key] !== undefined )
                {
                    self.$handlers[key].forEach( function( obj )
                    {
                      try
                      {
                          obj( mw ,  route );
                      }
                      catch( e )
                      {
                        reject( e );
                        return;
                      }
                    });
                }
              } );

              resolve('all is ok with middleware');

          } );


        return promise;

    };


  }
]);

/**
 * Created by coichedid on 21/04/15.
 */
'use strict';

 angular.module('dt-route-middleware').service( 'RouteMiddlewareHelper' ,  RouteMiddlewareHelper );

  RouteMiddlewareHelper.$inject = [ 'RouteMiddlewareHandler' ];

  function RouteMiddlewareHelper( RouteMiddlewareHandler )
  {
      
      var self = this;
      
      this.$handlers = {};

      this.$params = {};

      this.init = function( params )
      {
          this.$params = params;
      };



      this.register = function(  middleware ,   $routeMiddlewareHandler )
      {

          if( angular.isArray( $routeMiddlewareHandler )  )
          {
              $routeMiddlewareHandler.forEach( function(t){
                  self.register( middleware , t);
              } );
              return;
          }


          if( self.$handlers[middleware] === undefined )
          {
              self.$handlers[middleware] = [];
          }

          self.$handlers[middleware].push(  $routeMiddlewareHandler  );
          
      };


      this.hasMiddleware = function( route ){

            if( route === undefined )
            {
                return false;
            }


            if( route.$$route === undefined )
            {
                return false;
            }


            var $route = route.$$route;

            if( $route.middleware === undefined )
            {
                return false;
            }

            var mw = $route.middleware;

            return mw;

      };

      this.handle = function( route )
      {

         var mw = self.hasMiddleware( route );
         return RouteMiddlewareHandler.handle( mw , route , self.$handlers );

      };

     
  }

